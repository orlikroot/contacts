package ua.dp.krotov.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ua.dp.krotov.ContactsApplication;
import ua.dp.krotov.model.Contact;

import java.util.List;

/**
 * Created by Евгений on 14.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = {ContactsApplication.class})
public class ContactServiceTests {

    @Autowired
    private ContactService contactService;

    @Test
    public void testGetFilteredContacts() {
        List<Contact> contacts = contactService.getFilteredContacts("test");
        for (Contact contact : contacts) {
            Assert.assertFalse(contact.getName().equals("test"));
        }
    }
}
