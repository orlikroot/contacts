
package ua.dp.krotov;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by Евгений on 14.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = {ContactsApplication.class})
public class ContactsApplicationTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setUp() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void testRequestParamRequired() throws Exception {

        this.mvc.perform(get("/hello/contacts")).andExpect(status().is(400));
    }

    @Test
    public void testFormatResponse() throws Exception {

        this.mvc.perform(get("/hello/contacts?nameFilter=")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    public void testFindByRegularExp() throws Exception {

        this.mvc.perform(
                get("/hello/contacts?nameFilter=^.*[0-9].*$"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"name\":\"test\"}]"));
    }
}


