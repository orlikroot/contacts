package ua.dp.krotov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.dp.krotov.dao.ContactDao;
import ua.dp.krotov.model.Contact;

import java.util.List;

/**
 * Created by Евгений on 14.12.2015.
 */
@Service
public class ContactService {

    @Autowired
    private ContactDao contactDao;

    public List<Contact> getFilteredContacts(String param) {
        return contactDao.getFilteredContacts(param);
    }
}
