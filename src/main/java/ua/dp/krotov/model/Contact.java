package ua.dp.krotov.model;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 * Created by evgen on 13.12.2015.
 */
@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private long id;


    @NaturalId
    @Column(name = "NAME", nullable = false)
    private String name;

    public Contact() {
    }

    public Contact(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }
}
