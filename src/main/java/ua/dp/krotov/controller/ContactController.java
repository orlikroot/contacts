package ua.dp.krotov.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.dp.krotov.dao.ContactDao;
import ua.dp.krotov.model.Contact;
import ua.dp.krotov.service.ContactService;

import java.util.List;

/**
 * Created by evgen on 13.12.2015.
 */
@RestController
@RequestMapping(value = "/hello")
public class ContactController {

    private static final Logger logger = Logger.getLogger(ContactController.class);

    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/contacts",
            method = RequestMethod.GET)
    public List<Contact> getContacts(@RequestParam("nameFilter") String param) {
        logger.info("Request ContactController (RequestMapping = /hello/contacts) with param nameFilter = " + param);
        return contactService.getFilteredContacts(param);
    }

}
