package ua.dp.krotov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;

import java.io.FileWriter;
import java.io.IOException;


@SpringBootApplication
@EnableCaching
public class ContactsApplication {



    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("contacts");
    }

    public static void main(String[] args) {
        try(FileWriter writer = new FileWriter("/home/evgen/import.sql", false))
        {
            // запись всей строки
            String text="";
            for (int i=10; i<10000; i++) {
                text += "insert into contact(name) values ('test" + i + " ');" +'\n' ;
            }
            writer.write(text);
            writer.append('\n');
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

        SpringApplication.run(ContactsApplication.class, args);
    }
}
