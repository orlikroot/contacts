package ua.dp.krotov.dao;

import org.apache.log4j.Logger;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import ua.dp.krotov.model.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by evgen on 13.12.2015.
 */
@Repository
public class ContactDao {

    private static final Logger logger = Logger.getLogger(ContactDao.class);
    private Matcher matcher;
    private Pattern pattern;
    private List<Contact> filteredContacts;

    @Autowired
    private SessionFactory sessionFactory;

    @Cacheable("contacts")
    public List<Contact> getFilteredContacts(String param) {
        logger.info("Request method getFilteredContacts with param = " + param);
        pattern = Pattern.compile(param);
        filteredContacts = new ArrayList<>();
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        ScrollableResults contacts = session.createQuery("FROM Contact")
                .setCacheMode(CacheMode.IGNORE)
                .setFetchSize(1000)
                .scroll(ScrollMode.FORWARD_ONLY);
        int count=0;
        while ( contacts.next() ) {
            Contact contact = (Contact) contacts.get(0);
            matcher = pattern.matcher(contact.getName());
            if (!matcher.find()) {
                filteredContacts.add(contact);
            }
            if ( ++count % 100 == 0 ) {
                session.clear();
            }
        }
        tx.commit();
        session.close();
        logger.info("Found " + filteredContacts.size() + " filtered contacts");
        return filteredContacts;

    }

}
